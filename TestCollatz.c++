// ------------------------------------
// projects/c++/collatz/TestCollatz.c++
// Copyright (C) 2018
// Glenn P. Downing
// David Vu
// ------------------------------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <sstream>  // istringtstream, ostringstream
#include <string>   // string
#include <utility>  // pair

#include "gtest/gtest.h"

#include "Collatz.h"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    string s("1 10\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   1);
    ASSERT_EQ(p.second, 10);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval_1) {
    const int v = collatz_eval(1, 10);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval_2) {
    const int v = collatz_eval(100, 200);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval_3) {
    const int v = collatz_eval(201, 210);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval_4) {
    const int v = collatz_eval(900, 1000);
    ASSERT_EQ(v, 174);
}

TEST(CollatzFixture, eval_5) {
    const int v = collatz_eval(10, 1);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval_6) {
    const int v = collatz_eval(200, 100);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval_7) {
    const int v = collatz_eval(210, 201);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval_8) {
    const int v = collatz_eval(1000, 900);
    ASSERT_EQ(v, 174);
}

TEST(CollatzFixture, eval_9) {
    const int v = collatz_eval(1, 999999);
    ASSERT_EQ(v, 525);
}

TEST(CollatzFixture, eval_10) {
    const int v = collatz_eval(999999, 1);
    ASSERT_EQ(v, 525);
}

TEST(CollatzFixture, eval_14) {
    const int v = collatz_eval(827878, 927350);
    ASSERT_EQ(v, 525);
}

TEST(CollatzFixture, eval_11) {
    const int v = collatz_eval(519344, 714476);
    ASSERT_EQ(v, 509);
}

TEST(CollatzFixture, eval_12) {
    const int v = collatz_eval(788044, 159966);
    ASSERT_EQ(v, 509);
}

TEST(CollatzFixture, eval_13) {
    const int v = collatz_eval(349686, 871626);
    ASSERT_EQ(v, 525);
}


// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream w;
    collatz_print(w, 1, 10, 20);
    ASSERT_EQ(w.str(), "1 10 20\n");
}

// -----
// isPowerOf2
// -----

TEST(CollatzFixture, is_power_of_2_1) {
    ASSERT_TRUE(is_power_of_2(2));
}

TEST(CollatzFixture, is_power_of_2_2) {
    ASSERT_TRUE(is_power_of_2(4));
}

TEST(CollatzFixture, is_power_of_2_3) {
    ASSERT_TRUE(is_power_of_2(16));
}

TEST(CollatzFixture, is_power_of_2_4) {
    ASSERT_TRUE(!is_power_of_2(6));
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream r("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", w.str());
}

TEST(CollatzFixture, solve2) {
    istringstream r("794952 814690\n255373 474129\n100241 801032\n689627 768487\n691069 964601\n890931 599308\n705651 934564\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("794952 814690 468\n255373 474129 449\n100241 801032 509\n689627 768487 504\n691069 964601 525\n890931 599308 525\n705651 934564 525\n", w.str());
}
