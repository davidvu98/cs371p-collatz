// --------------------------------
// projects/c++/collatz/Collatz.c++
// Copyright (C) 2018
// Glenn P. Downing
// David Vu
// --------------------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair
#include <string.h>
#include <math.h>
#include "Collatz.h"

using namespace std;

#define CACHE_SIZE 1000000
#define EAGER_CACHE_SIZE 500000

int CACHE[CACHE_SIZE];




// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}


// ------------
// is_power_of_2
// ------------

bool is_power_of_2(unsigned long x) {
    return (x & (x - 1)) == 0;
}



// ------------
// get_cycle_length
// ------------
/*
    recursively gets the cycle length of i
    and cache the cycle length of all other numbers
    along its path to 1
*/
int get_cycle_length(unsigned long i) {
    if(i == 1 || (i <= CACHE_SIZE && CACHE[i - 1] != 0))
        return CACHE[i - 1];
    else {
        /*optimization*/
        //The cycle length of 2^n is n + 1
        if(is_power_of_2(i))
            return 1 + int(log2(i));

        int a = 0;
        int *cycle_length;
        if(i <= CACHE_SIZE)
            cycle_length = &(CACHE[i-1]);
        else
            cycle_length = &a;

        if(!(i % 2)) // even
            *cycle_length = 1 + get_cycle_length(i >> 1);
        else
            *cycle_length = 2 + get_cycle_length((i * 3 + 1) >> 1); //skip *3 +1 cycle
        return *cycle_length;
    }
}

// ------------
// collatz_eval
// ------------
int collatz_eval (int i, int j) {

    //swap i j
    if(i > j) {
        int temp = i;
        i = j;
        j = temp;
    }

    //optimization
    //max_cycle_length(i,j) == max_cycle_length(j / 2 + 1, j) when i < j / 2 + 1
    if(i < (j >> 1) + 1)
        i = (j >> 1) + 1;


    int max_cycle_lengh = get_cycle_length(i);
    ++i;
    while(i <= j) {
        int cycle_length = get_cycle_length(i);
        if(max_cycle_lengh < cycle_length)
            max_cycle_lengh = cycle_length;
        i++;
    }

    return max_cycle_lengh;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    int i;
    //zero out cache
    memset(CACHE, 0, CACHE_SIZE * sizeof(int));
    CACHE[0] = 1;


    //eager caching all log base 2
    //cycle_length(2^n) == cycle_length(2^(n-1)) + 1
    for(i = 2; i < CACHE_SIZE; i*=2)
        CACHE[i - 1] = CACHE[(i-1) / 2] + 1;


    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
